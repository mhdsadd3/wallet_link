## CLONE ##

# run "git clone https://gitlab.com/mhdsadd3/wallet_link"


## TO RUN LOCALLY ##

#Clone app and open with favorite text editor
#From the *app.js* file in the root directory run **"npm install"** or **"sudo npm install"** if admin priviledge is required
#Make sure to have **mongoDB** install and it's GUI app **MONGODB COMPASS** for easy local access



##  TO DEPLOY TO PRODUCTION ##

#Make sure to change the local **MONGO_URI** value in *.env* from localhost to it's **mongoDB ATLAS connection string**


## SEED ADMIN IN DATABASE ##

#To seed admin into the database go into the path */seeder* with **"cd seeder"**
#Once inside the seeder directory open the *adminSeeder* file and change the admin instance information to your preferance
#Then run the command **"node adminSeeder"** from the path */seeder* and make sure the admin has been saved when shown the log **admin saved successfully**
#Check DB to make sure, then you can use the information PASSWORD and EMAIL to login the admin dashboard
#To go back the root directory do **"cd .."**

## TO ADD MORE REDIRECT LINK ##
#Go into *controller/userController.js* 
#Locate the Link module and after the *newWallte.save()* you'll see where to add the redirect using **if else** condition
