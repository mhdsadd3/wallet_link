const express = require("express");
const mongoose = require("mongoose");
const passport = require("passport");
const { Admin } = require("../models/admin");
const { Link } = require("../models/link_model");


  module.exports.dashboard = async (req, res) => {
    let pagetitle = "Dashboard";
    let name = req.user.user_name;
    let email = req.user.email;
    res.render("admin/dashboard", { pagetitle, name, email });
  }

  module.exports.wallets_table = async (req, res) => {
    let pagetitle = "Wallets";
    const wallets = await Link.find();
    const wallet_count = await Link.countDocuments();
    let name = req.user.user_name;
    let email = req.user.email;
    res.render("admin/wallets", { pagetitle, wallets, wallet_count, name, email });
  }
