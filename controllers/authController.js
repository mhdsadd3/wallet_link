const express = require("express");
const passport = require("passport")
const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const { User } = require("../models/user_model");
const { Link } = require("../models/link_model");


// Registration & Login GET Module
module.exports.user_register_loginGET = (req, res) => {
  const pagetitle = "Auth";
  res.render("auth/register_login", { pagetitle });
};

// Registration POST Module
module.exports.user_registerPOST = (req, res, err) => {
  const { user_name, email, password } = req.body;

  let errors = [];
  if (!user_name || !email || !password) {
    errors.push({ msg: "Please fill in all fields" });
  }
  if (password.length < 4) {
    errors.push({ msg: "Password must be atleast 4 Characters" });
  }
  if (errors.length > 0) {
    let pagetitle = "Auth";
    res.render("auth/register_login", {
      errors,
      user_name,
      email,
      password,
      pagetitle,
    });
  } else {
    User.findOne({ email: email }).then((user) => {
      if (user) {
        errors.push({ msg: "Email is already registered" });
        let pagetitle = "Auth";
        req.flash("error_msg", `User already exists`);
        res.render("auth/register_login", {
          errors,
          user_name,
          email,
          password,
          pagetitle,
        });
      } else {
        const newUser = new User({
          user_name,
          email,
          password,
        });

        bcrypt.genSalt(10, (err, salt) =>
          bcrypt.hash(newUser.password, salt, (err, hash) => {
            if (err) throw err;

            // SETTING PASSWORD TO HASH
            newUser.password = hash;
            // SAVING USER
            newUser.save();
            req.flash("success_msg", "Wallet link registration successfull");
            res.redirect("/auth/register");
          })
        );
      }
    });
  }
};



// userLogin Module
// module.exports.user_loginPOST = (req, res, next) => {
//   passport.authenticate("local", {
//     successRedirect: "/",
//     failureRedirect: "/auth/register",
//     failureFlash: true,
//   })(req, res, next);
// };

// userLogin Module
module.exports.admin_loginPOST = (req, res, next) => {
  passport.authenticate("local", {
    successRedirect: "/admin/dashboard",
    failureRedirect: "/auth/register",
    failureFlash: true,
  })(req, res, next);
};


// Logout Module

module.exports.admin_logout = (req, res) => {
  req.logOut();
  req.flash('success_msg', "You are logged out");
  res.redirect("/");
}
