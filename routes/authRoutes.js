const router = require("express").Router()
const auth_controller = require('../controllers/authController')
const admin_controller = require('../controllers/adminController')
const ensureIsAuthenticated = require("../config/auth")


// Register Route
router.route("/register")
.get(auth_controller.user_register_loginGET)
.post(auth_controller.user_registerPOST);


// Login Route
router.route("/login")
.get(auth_controller.user_register_loginGET)
.post(auth_controller.admin_loginPOST)


// Logout Handle
router.get("/logout", auth_controller.admin_logout)






module.exports = router;