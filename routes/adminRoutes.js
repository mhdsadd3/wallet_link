const router = require("express").Router()
const admin_controller = require('../controllers/adminController')
const ensureIsAuthenticated = require("../config/auth")



router.get('/dashboard', ensureIsAuthenticated, admin_controller.dashboard)
router.get('/wallets', ensureIsAuthenticated, admin_controller.wallets_table)



module.exports = router