const router = require("express").Router();
const index_controller = require("../controllers/userController");
// const ensureIsAuthenticated = require("../config/auth")

// Home Route
router.route("/").get(index_controller.Index);

// Services Route
router.route("/services").get(index_controller.Services);

// About Route
router.route("/about").get(index_controller.About);

// Contact Route
router.route("/contact").get(index_controller.Contact);

//  Wallet Route
router.route("/wallets").get(index_controller.Wallets);

// Wallet_Link Route
router.post("/link", index_controller.link_walletPOST);

module.exports = router;
