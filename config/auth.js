module.exports = (req, res, next) => {
  if (req.isAuthenticated()) {
    return next();
  } else {
    req.flash("error_msg", "You have to login first");
    res.redirect("/auth/register");
  }
};
