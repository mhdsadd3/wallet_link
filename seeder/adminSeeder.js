
const MONGO_URI = "mongodb://localhost/walletLink";
const {Admin} = require("../models/admin");
const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");


// connecting to MongoDB with
mongoose
  .connect(MONGO_URI, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log(`DB CONNECTED SUCCESSFULLY:::`);
  })
  .catch((err) => {
    console.log(err);
  });

const admin = new Admin({
  user_name: "ADMIN",
  email: "walletlink@gmail.com",
  phone: 08068640710,
  password: "123abc",
  user_type: "Admin",
});

bcrypt.genSalt(10, (err, salt) => {
  bcrypt.hash(admin.password, salt, (err, hash) => {
    if (err) {
      
      throw err;
    }
    admin.password = hash;
    admin
      .save()
      .then(() => {
        console.log(admin)
        console.log("admin saved successfully");
      })
      .catch((err) => {
        console.log(err);
      });
  });
});
