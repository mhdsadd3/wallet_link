require("dotenv").config();

const express = require("express");
const app = express();
const logger = require("morgan");
const ejs = require("ejs");
const path = require('path')
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const session = require("express-session");
const MongoStore = require('connect-mongo');
const flash = require("connect-flash");
const passport = require("passport");
const mongoose = require("mongoose");
const {MONGO_URI, globalVariables} =  require('./config/configurations')
require("./config/passport")(passport);



// connect DB 
mongoose.connect(MONGO_URI, {
  useCreateIndex: true,
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false
})
.then((res) => {
  console.log(`Database Connected at MongoURI...`)
})
.catch((err) => {
  console.log(`Database connection failed ${err}`)
})

// morgan init
app.use(logger('dev'))

// setting up template engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// Connecting to static files
app.use(express.static(path.join(__dirname,'public')));

// cookie parser init
app.use(cookieParser());

//Configure Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));


// SET UP EXPRESS_SESSION MIDDLEWARE
app.use(
  session({
      secret: process.env.NODE_SECRET,
      resave: false,
      saveUninitialized: false,
      cookie: { secure: false, maxAge: Date.now() + 3600000},
      store: MongoStore.create({
        mongoUrl: MONGO_URI
    })
  })
);

//initiallize passport
app.use(passport.initialize());
app.use(passport.session());

// flash init
app.use(flash())

// globalvariables Init
app.use(globalVariables)


// routing
const defaultRoutes = require("./routes/userRoutes");
const auth = require("./routes/authRoutes");
const admin = require("./routes/adminRoutes")

app.use("/", defaultRoutes);
app.use("/auth", auth);
app.use("/admin", admin)




// Error Handler
app.use(( req, res, next) => {
  let pagetitle = "404";
  res.render("error_page", {pagetitle})
  next()
})


app.listen(process.env.PORT || 1999, () => {
  console.log(`Server on ${process.env.HOSTNAME}${process.env.PORT}`)
});