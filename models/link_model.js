const mongoose = require("mongoose");
const { Schema } = mongoose;

const linkSchema = new Schema({
  wallet_id: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    // required: true,
  },
  wallet_name: {
    type: String,
    // required: true,
  },
});

module.exports = {Link: mongoose.model('link', linkSchema)}

