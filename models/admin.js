const mongoose = require("mongoose");
const { Schema } = mongoose;

const adminSchema = new Schema({
  user_name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  user_type: {
    type: String,
    required: true,
  },
});
module.exports ={ Admin: mongoose.model("admin", adminSchema)}